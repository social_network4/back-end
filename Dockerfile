FROM python:3.8

RUN which python
RUN python --version

WORKDIR /app
ADD . .

RUN pip install -r /app/requirements.txt

ENV PYTHONPATH=/app:/app

EXPOSE 5000

CMD ["python", "--version" ]
CMD ["python", "main.py" ]
