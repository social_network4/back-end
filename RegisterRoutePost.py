import logging
import os

from bson import ObjectId
from flask import request, jsonify
from flask_cors import cross_origin

import settings
from auth_middleware import token_required
from models.Image import Image
from models.Post import Post
from models.Like import Like
from models.Comment import Comment

def registerPostRoute(app):
    @app.route("/posts/<id>", methods=["DELETE"])
    @token_required
    @cross_origin()
    def deletePost(currentUser, id):
        postId = Post.deletePost(id,currentUser.get("_id",""))

        return jsonify({
            "status": "ok",
            "message": "successfully create like post",
            "data": postId
        })

    @app.route("/posts/comment", methods=["POST"])
    @token_required
    @cross_origin()
    def createComment(currentUser):
        data = request.json
        cmts = Comment()
        cmts.__int__(comment=data.get("comment",""),postId=data.get("tusId",""),userId=currentUser.get("_id",""))
        rs = cmts.createComment()
        return jsonify({
            "status": "ok",
            "message": "successfully create cmt",
            "data": rs
        })

    @app.route("/posts/myPosts", methods=["GET"])
    @token_required
    @cross_origin()
    def getMyPosts(currentUser):
        res = Post.getPostByUserId(currentUser.get("_id",""))
        return jsonify({
            "status": "ok",
            "message": "successfully retrieved posts data",
            "data": res
        }), 200

    @app.route("/posts/listComment/<id>", methods=["GET"])
    @cross_origin()
    def getListCmtByPostId(id):
        cmts = Comment.getComment(id)
        return jsonify({
            "status": "ok",
            "message": "successfully get cmt",
            "data": cmts
        })

    @app.route("/posts/like/<id>", methods=["GET"])
    @token_required
    @cross_origin()
    def likePost(currentUser,id):
        liker = Like()
        liker.__int__(userId=currentUser.get("_id",""),postId=id)
        rs = liker.createLike()

        return jsonify({
            "status": "ok",
            "message": "successfully create like post",
            "data": rs
        })

    @app.route("/posts/new", methods=["POST"])
    @token_required
    @cross_origin()
    def newPost(currentUser):
        print(currentUser)
        try:

            if not os.path.isdir(settings.UPLOADS_PATH):
                os.mkdir(settings.UPLOADS_PATH)
            if "files" in request.files:
                fileUp = request.files["files"]
                caption = request.form.get("caption", "")
                filename, file_extension = os.path.splitext(fileUp.filename)
                # print(fileUp.content_type,fileUp.mimetype,fileUp.filename,file_extension)
                objID = ObjectId()

                path = os.path.join(settings.UPLOADS_PATH, f"{objID}{file_extension}")
                objImg = Image()
                imgUrl = "{}/static/img/"
                imgUrl += str(objID)
                imgUrl += file_extension
                objImg.__int__(_id=str(objID), image=imgUrl, userIdUpload=str(currentUser.get("_id", "")))
                rsCreate = objImg.createImage()
                fileUp.save(path)
                postObj = Post()
                postObj.__int__(caption=caption,
                                postsUserList=[
                                    {"postsUserId": str(currentUser.get("_id", "")),
                                     "userId": str(currentUser.get("_id", "")),
                                     "name": f"{currentUser.get('firstName')} {currentUser.get('lastName')}",
                                     "image": currentUser.get("avatar"),

                                     "dateCreate": currentUser.get("dateCreate", "")
                                     }
                                ],
                                postsImageList=[rsCreate])
                idPost = postObj.createNewPost()
                return jsonify({
                    "status": "ok",
                    "message": "successfully create new post",
                    "data": idPost
                })
            else:
                return {
                           "status": "error",
                           "error": "Bad request",
                           "message": "No file found in request",
                           "data": None
                       }, 400
        except Exception as e:
            logging.exception(e)
            return {
                       "status": "error",
                       "message": "created new fail",
                       "data": None
                   }, 500

    @app.route("/posts/user/<id>", methods=["GET"])
    @cross_origin()
    def getPostByUserID(id):
        res = Post.getPostByUserId(id)
        return jsonify({
            "status": "ok",
            "message": "successfully retrieved user profile",
            "data": res
        }), 200

    @app.route("/posts/<id>", methods=["GET"])
    @cross_origin()
    def getPostByID(id):
        res = Post.getPostById(id)
        return jsonify({
            "status": "ok",
            "message": "successfully retrieved user profile",
            "data": res
        }), 200

    @app.route("/posts/show", methods=["GET"])
    @cross_origin()
    @token_required
    def showAllPost(currentUser):
        res = Post.getPost(userId=currentUser.get("_id",""))

        return jsonify({
            "status": "ok",
            "message": "successfully retrieved user profile",
            "data": res
        }), 200
