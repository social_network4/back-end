import psycopg2
import settings
import logging

def getConnection():
    conn = psycopg2.connect(
        database=settings.POSTGRESS_DBNAME,
        user=settings.POSTGRESS_USER,
        password=settings.POSTGRESS_PASS,
        host=settings.POSTGRESS_HOST,
        port=settings.POSTGRESS_PORT,
    )
    if conn is not None:
        return conn
    else:
        return None

def initDB():
    createTableUser()

def createTableUser():
    conn = getConnection()
    if conn == None:
        print("user", "False")
        return
    try:
        cur = conn.cursor()
        sql = """
        CREATE TABLE IF NOT EXISTS "user"
        (
            username text COLLATE pg_catalog."default",
            id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
            password text COLLATE pg_catalog."default",
            name text COLLATE pg_catalog."default",
            phone text COLLATE pg_catalog."default",
            sex text COLLATE pg_catalog."default",
            email text COLLATE pg_catalog."default",
            active boolean DEFAULT false,
            created_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
            CONSTRAINT user_pkey PRIMARY KEY (id)
        )
    
        """
        cur.execute(sql)
        conn.commit()
        conn.close()
        cur.close()
    except Exception as e:
        logging.exception(e)

