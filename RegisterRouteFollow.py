from auth_middleware import token_required
from flask_cors import cross_origin
from models.Follow import Follow
from flask import request, jsonify

def registerRouteFollow(app):
    @app.route("/follow/send/<id>", methods=["GET"])
    @token_required
    @cross_origin()
    def sendFollow(current_user, id):
        follow = Follow()
        follow.__int__(followerId=current_user.get("_id",""),followedId=id)
        rs = follow.createOrDeleteFollow()
        return jsonify({
            "status": "ok",
            "message": "successfully create follow",
            "data": rs
        })