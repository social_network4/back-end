from Utils.SQL_Helper import getConnection
import logging
import psycopg2.extras

class User:
    def __init__(self,username="",password="",name="",phone="",email="",sex="",user_id=""):
        self.username = username
        self.password = password
        self.name = name
        self.phone = phone
        self.email = email
        self.sex = sex



    def createUser(self):
        conn = getConnection()
        if not conn:
            return False
        else:
            try:
                cur = conn.cursor()
                listKey = ["username", "password", "name", "phone", "email", "sex"]
                tupleValue = (self.username, self.password, self.name, self.phone,self.email, self.sex)

                sql = (f'INSERT INTO "user"({",".join(listKey)}) '
                       f'VALUES ({",".join(["%s" for i in range(len(listKey))])})')

                cur.execute(sql, tupleValue)
                conn.commit()
                cur.close()
                conn.close()
                return True
            except (Exception, psycopg2.Error) as error:
                logging.exception(error)
                return False
    def login(self,username,password):
        conn = getConnection()
        if conn == None:
            return None
        try:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            sql = f'SELECT * FROM "user" WHERE username = %s AND password = %s'
            cur.execute(sql, (username, password))
            response = list(cur.fetchall())
            if len(response)!=0:
                output = dict(response[0])
                cur.close()
                conn.close()
                print("login", output)
                return output
            else:
                cur.close()
                conn.close()

                return None
        except Exception as e:
            logging.exception(e)
            return None
    def validate_email_and_password(self,username,password):
        conn = getConnection()
        if conn == None:
            return False
        try:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            sql = f'SELECT * FROM "user" WHERE username = %s AND password = %s'
            cur.execute(sql, (username,password))
            response = cur.fetchall()
            output = list(response)[0]
            cur.close()
            conn.close()
            print("login", output)
            if output is not None:
                return True
            else:
                return False
        except Exception as e:
            logging.exception(e)
            return False
    def get_by_id(self,user_id):
        conn = getConnection()
        if conn == None:
            return None
        try:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            sql = f'SELECT * FROM "user" WHERE id = %s'
            cur.execute(sql, (user_id,))
            response = cur.fetchall()

            output = dict(response[0])
            cur.close()
            conn.close()
            print("get_by_id", output)
            return output
        except Exception as e:
            logging.exception(e)
            return None

