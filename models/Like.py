from bson import ObjectId
import datetime
from db import db


class Like:
    def __int__(self, _id: str = str(ObjectId), postId: str = "", dateTime: str = "", userId: str = ""):
        self._id = _id
        self.postId = postId
        self.dateTime = dateTime
        self.userId = userId

    @classmethod
    def getAllLikeByPostId(cls,id):
        likeList = list(db["like"].find({"postId": id},{"_id":0}))
        return likeList

    @classmethod
    def isUserLiked(cls,uid,postid):
        numlike = db["like"].count_documents({"userId":uid,"postId":postid})
        if numlike == 0:
            return False
        else:
            return True

    def createLike(self):
        likers = db["like"].find_one({"postId": self.postId, "userId": self.userId})
        if not likers:
            db["like"].insert_one({"postId": self.postId, "userId": self.userId,"dateTime":str(datetime.datetime.now())})
            return True
        else:
            db["like"].delete_one({"postId": self.postId, "userId": self.userId})
            return False