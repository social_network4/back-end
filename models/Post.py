import datetime
from bson import ObjectId
from db import db
from flask import request
from models.Like import Like
from models.Userv2 import Userv2Class
class Post:
    def __int__(self, _id="", caption="", totalFeel=0, totalComment=0, dateCreate="", feel=False, postsUserList=[],
                postsCommentList=[], postsFeelList=[], postsImageList=[]):
        self._id = _id
        self.caption = caption
        self.totalFeel = totalFeel
        self.totalComment = totalComment
        self.dateCreate = dateCreate
        self.feel = feel
        self.postsUserList = postsUserList
        self.postsCommentList = postsCommentList
        self.postsFeelList = postsFeelList
        self.postsImageList = postsImageList

    @classmethod
    def deletePost(cls,id,userId):
        db["post"].delete_one({"_id":ObjectId(id),"postsUserList":{"$elemMatch": {"userId":userId}}})
        return id

    @classmethod
    def like(cls,id,currentUser):
        userLike = db["post"].find_one({"_id":ObjectId(id),"postsFeelList":{"$elemMatch": {"userId":currentUser.get("_id","")}}})
        isLike = False
        if not userLike:
            db["post"].insert_one({"_id":ObjectId(id)},
                                  {"$push":{"postsFeelList":
                                      {
                                          "userId":currentUser.get("_id",""),
                                          "name":f"{currentUser.get('firstName')} {currentUser.get('lastName')}",
                                          "image":currentUser.get("image")
                                      }}})


    @classmethod
    def getPostByUserId(cls,id="",limit=30,skip=0):
        posts = list(db["post"].find({"postsUserList":{"$elemMatch": {"userId":id}}}).limit(limit).skip(skip))
        for item in posts:
            item["postsId"] = str(item.get("_id",""))
            item["_id"] = str(item.get("_id",""))
            liker = Like()
            rsLiker = liker.getAllLikeByPostId(str(item.get("_id","")))
            item["totalFeel"] = len(rsLiker)
            item["postsFeelList"] = rsLiker
            if "postsUserList" in item:
                for us in item["postsUserList"]:
                    userPost = Userv2Class.get_user_by_id(us.get("userId", ""))
                    us["image"] = userPost.get("avatar", "")
                    us["dateCreate"] = userPost.get("dateCreate", "2022-09-06")
                    us["name"] = userPost.get("lastName","")
                    if "{}" in userPost["avatar"]:
                        us["image"] = userPost["avatar"].format(request.host_url)
            if "postsImageList" in item:
                for img in item["postsImageList"]:
                    if "{}" in img["image"]:
                        img["image"] = img["image"].format(request.host_url)

        return posts

    @classmethod
    def getPostById(cls,id:str = "629f10bb8901e907d5c36929"):
        postX = db["post"].find_one({"_id":ObjectId(id)})
        postX["postsId"] = str(postX.get("_id", ""))
        postX["_id"] = str(postX.get("_id", ""))
        liker = Like()
        rsLiker = liker.getAllLikeByPostId(str(postX.get("_id", "")))
        postX["totalFeel"] = len(rsLiker)
        postX["postsFeelList"] = rsLiker
        if "postsUserList" in postX:
            for us in postX["postsUserList"]:
                userPost = Userv2Class.get_user_by_id(us.get("userId", ""))
                us["image"] = userPost.get("avatar", "")
                us["dateCreate"] = userPost.get("dateCreate", "2022-09-06")
                us["name"] = userPost.get("lastName", "")
                if "{}" in userPost["avatar"]:
                    us["image"] = userPost["avatar"].format(request.host_url)
        if "postsImageList" in postX:
            for img in postX["postsImageList"]:
                if "{}" in img["image"]:
                    img["image"] = img["image"].format(request.host_url)
        return postX

    @classmethod
    def getPost(cls, userId:str = "",limit: int = 20, skip: int = 0)->list:
        posts = list(db["post"].find({}).sort("dateCreate",-1).limit(limit).skip(skip))
        for item in posts:
            item["postsId"] = str(item.get("_id",""))
            item["_id"] = str(item.get("_id",""))
            liker = Like()
            rsLiker = liker.getAllLikeByPostId(str(item.get("_id", "")))
            islike = liker.isUserLiked(uid=userId,postid=str(item.get("_id", "")))
            item["totalFeel"] = len(rsLiker)
            item["postsFeelList"] = rsLiker
            item["feel"] = islike
            if "postsUserList" in item:
                for us in item["postsUserList"]:
                    userPost = Userv2Class.get_user_by_id(us.get("userId",""))
                    print(us.get("userId",""))
                    us["image"] = userPost.get("avatar","")
                    us["name"] = userPost.get("lastName","")
                    us["dateCreate"] = userPost.get("dateCreate","2022-09-06")
                    if "{}" in userPost["avatar"]:
                        us["image"] = userPost["avatar"].format(request.host_url)
            if "postsImageList" in item:
                for img in item["postsImageList"]:
                    # print(str(img["image"]))
                    if "{}" in str(img["image"]):
                        img["image"] = img["image"].format(request.host_url)
        return posts
    def createNewPost(self):
        newPostObj = {
            "caption": self.caption,
            "dateCreate": str(datetime.datetime.now()),
            "postsUserList":self.postsUserList,
            "totalComment":0,
            "totalFeel":0,
            "feel":self.feel,
            "postsCommentList":self.postsCommentList,
            "postsFeelList":self.postsFeelList,
            "postsImageList":self.postsImageList
        }
        db["post"].insert_one(newPostObj)
        return str(newPostObj.get("_id",""))
