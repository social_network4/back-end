import datetime

from bson import ObjectId
from db import db
from models.Userv2 import Userv2Class

class Comment:
    def __int__(self, _id: str = str(ObjectId()), comment: str = "", postId: str = "", userId: str = "",
                dateCreate: str = str(datetime.datetime.now())):
        self._id = _id
        self.comment = comment
        self.postId = postId
        self.userId = userId
        self.dateCreate = dateCreate

    def createComment(self,):
        oid = ObjectId()
        db["comment"].insert_one({"_id":oid,"postsCommentId":str(oid),"comment":self.comment,"postId":self.postId,"userId":self.userId,"dateCreate":self.dateCreate})
        return str(oid)

    @classmethod
    def getComment(cls,postId):
        lcomment = list(db["comment"].find({"postId":postId},{"_id":0}))
        for cmt in lcomment:
            userOfCmt = Userv2Class.get_user_by_id(cmt["userId"])
            cmt["image"] = userOfCmt.get("avatar","")
            cmt["name"] = userOfCmt.get("lastName","")+" "+userOfCmt.get("firstName","")
        # print(lcomment)
        return lcomment

