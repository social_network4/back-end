import datetime
from bson import ObjectId
from db import db

class Image:
    def __int__(self,_id:str = str(ObjectId()),image:str = "",dateCreate:str="",userIdUpload:str=""):
        self._id = _id
        self.image = image
        self.dateCreate = dateCreate
        self.userIdUpload = userIdUpload

    def createImage(self)->dict:
        obj = {"_id":ObjectId(self._id),"postsImageId":self._id,"image":self.image,"dateCreate":str(datetime.datetime.now()),"userIdUpload":self.userIdUpload}
        db["image"].insert_one(obj)
        obj["_id"] = str(obj["_id"])
        obj.pop("_id",None)
        return obj